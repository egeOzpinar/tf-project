I want to keep this project simple so I don't create EKS cluster and used just EC2 instances and Autoscaling Groups.

I created 2 public subnets and 1 private subnet. In order to improve HA, I created 2 public subnets for frontend application. To have the load balancer open to the internet, I placed it in the public subnet.

I created 2 security groups, for frontend and for backend. The security group for frontend app can be accessible by load balancer. The security group for backend can be accessible by frontend applications and just connect to allowed ip-list/port.

These resources above can be provisioned with these following commands:
1. ```terraform init```

2. ```terraform apply```
